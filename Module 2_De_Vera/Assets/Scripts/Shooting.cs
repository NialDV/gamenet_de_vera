﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class Shooting : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject hitEffectPrefab;

    [Header("HP Related Stuff")]
    public float startHealth = 100;
    private float health;
    public Image healthBar;

    private Animator animator;

    public int killCount;
    public string lastHitter;


    // Start is called before the first frame update
    void Start()
    {
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
        animator = this.GetComponent<Animator>();
    }

    public void Fire()
    {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if (Physics.Raycast(ray, out hit, 200))
        {
            Debug.Log(hit.collider.gameObject.name);

            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);

            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25);
            }
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.health -= damage;
        this.healthBar.fillAmount = health / startHealth;

        if (health <= 0)
        {
            Die(info);
            GameObject killFeed = GameObject.Find("KillFeed");
            killFeed.GetComponent<Text>().text = info.Sender.NickName + " killed " + info.photonView.Owner.NickName;
            StartCoroutine(TimerUI(killFeed));
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
        }
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }

    public void Die(PhotonMessageInfo info)
    {
        if (photonView.IsMine)
        {
            animator.SetBool("isDead", true);
            StartCoroutine(RespawnCountDown());
        }
        else 
        {
            killCount++; // take damage
            lastHitter = info.Sender.NickName;

            GameObject killCountUi = GameObject.Find("KillCountUi");
            killCountUi.GetComponent<Text>().text = "Kill Count: " + killCount.ToString("");

            if (killCount == 10)
            {
                transform.GetComponent<PlayerMovementController>().enabled = false;
                photonView.RPC("DisplayWinner", RpcTarget.AllBuffered, lastHitter);
            }
        }
    }

    [PunRPC]
    public void DisplayWinner(string lastHitter)
    {
        GameObject winnerUi = GameObject.Find("WinnerUi");
        winnerUi.GetComponent<Text>().text = lastHitter + " is the Winner!";
    }

    [PunRPC]
    IEnumerator RespawnCountDown()
    {
        GameObject respawnText = GameObject.Find("Respawn Text");
        float respawnTime = 5.0f;
        
        while(respawnTime > 0)
        {
            yield return new WaitForSeconds(1.0f);
            respawnTime--;

            transform.GetComponent<PlayerMovementController>().enabled = false;
            respawnText.GetComponent<Text>().text = "You are Killed. Respawning in " + respawnTime.ToString(".00");
        }

        animator.SetBool("isDead", false);
        respawnText.GetComponent<Text>().text = "";

        SpawnManager.Instance.RandomSpawnPoint(this.gameObject);

        transform.GetComponent<PlayerMovementController>().enabled = true;

        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);
    }

    [PunRPC]
    IEnumerator TimerUI(GameObject killFeed)
    {
        yield return new WaitForSeconds(3.5f);
        killFeed.GetComponent<Text>().text = "";
    }

    [PunRPC]
    public void RegainHealth()
    {
        health = 100;
        healthBar.fillAmount = health / startHealth;
    }
}
