﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectFire : MonoBehaviour
{
    List<GameObject> players = new List<GameObject>();
    
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            players.Add(col.gameObject);
            StartCoroutine(DamageFromFire(col.gameObject.GetComponent<Shooting>()));
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            players.Remove(col.gameObject);

        }
    }

    IEnumerator DamageFromFire(Shooting shooting)
    {
        while(players.Contains(shooting.gameObject))
        {
            yield return new WaitForSeconds(1);
            shooting.health -= 5;
            shooting.healthBar.fillAmount = shooting.health / shooting.startHealth;
        }
    }
}
