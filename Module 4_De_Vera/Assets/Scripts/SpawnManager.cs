﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SpawnManager : MonoBehaviour
{
    public static SpawnManager Instance;
    public GameObject[] spawnPoints;

    private void Awake()
    {
        if (Instance = null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    public void RandomSpawnPoint(GameObject player)
    {
        player.transform.position = spawnPoints[Random.Range(0, 1)].transform.position;
    }
}
