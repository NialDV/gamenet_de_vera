﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;
using UnityStandardAssets.Characters.FirstPerson;

public class Shooting : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject hitEffectPrefab;

    [Header("HP Related Stuff")]
    public float startHealth = 100;
    public float health;
    public Image healthBar;

    public System.Action<GameObject> winEvent;

    private Animator animator;

    public int killCount;
    public string lastHitter;

    // Start is called before the first frame update
    void Start()
    {
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
        animator = this.GetComponent<Animator>();
    }

    void Update()
    {
        Fire();
    }

    public void Fire()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

            if (Physics.Raycast(ray, out hit, 200))
            {
                Debug.Log(hit.collider.gameObject.name);

                photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);

                if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
                {
                    hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 10);
                }
            }
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.health -= damage;
        this.healthBar.fillAmount = health / startHealth;

        if (health <= 0)
        {
            Die(info);
            GameManager.Instance.killFeed.GetComponent<TextMeshProUGUI>().text = info.Sender.NickName + " killed " + info.photonView.Owner.NickName;
            StartCoroutine(TimerUI(GameManager.Instance.killFeed));
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
        }
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }

    public void Die(PhotonMessageInfo info)
    {
        if (photonView.IsMine)
        {
            animator.SetBool("isDead", true);
            Respawn();
        }
        else
        {
            killCount++; // take damage
            lastHitter = info.Sender.NickName;

            GameManager.Instance.killCountUi.GetComponent<TextMeshProUGUI>().text = "Kill Count: " + killCount.ToString("");

            if (killCount == 2)
            {
                GetComponent<RigidbodyFirstPersonController>().enabled = false;
                photonView.RPC("DisplayWinner", RpcTarget.AllBuffered, lastHitter);

                GameManager.Instance.winnerUi.SetActive(true);
                winEvent.Invoke(gameObject);
            }
        }
    }

    public void Respawn()
    {
        StartCoroutine(RespawnCountDown());
    }

    [PunRPC]
    public void DisplayWinner(string lastHitter)
    {
        GameManager.Instance.winnerUi.GetComponent<TextMeshProUGUI>().text = lastHitter + " is the Winner!";
    }

    IEnumerator RespawnCountDown()
    {
        float respawnTime = 5.0f;

        while (respawnTime > 0)
        {
            GameManager.Instance.respawnText.GetComponent<TextMeshProUGUI>().text = "You are Killed. Respawning in " + respawnTime.ToString(".00");
            yield return new WaitForSeconds(1.0f);
            respawnTime--;
            GetComponent<RigidbodyFirstPersonController>().enabled = false;
        }
        animator.SetBool("isDead", false);

        GameManager.Instance.respawnText.GetComponent<TextMeshProUGUI>().text = "";
        SpawnManager.Instance.RandomSpawnPoint(this.gameObject);

        GetComponent<RigidbodyFirstPersonController>().enabled = true;

        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);
    }

    [PunRPC]
    IEnumerator TimerUI(GameObject killFeed)
    {
        yield return new WaitForSeconds(3.5f);
        GameManager.Instance.killFeed.GetComponent<TextMeshProUGUI>().text = "";
    }

    [PunRPC]
    public void RegainHealth()
    {
        health = 100;
        healthBar.fillAmount = health / startHealth;
    }
}
