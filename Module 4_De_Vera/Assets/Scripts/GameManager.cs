﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class GameManager : MonoBehaviourPunCallbacks
{
    public GameObject playerPrefab;
    public GameObject respawnText;
    public GameObject killFeed;
    public GameObject killCountUi;
    public GameObject winnerUi;

    public GameObject gameOverPanel;

    public static GameManager Instance;

    private void Awake()
    {
        if (Instance = null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            int randomPointZ = Random.Range(-5, -15);
            Shooting shooting = PhotonNetwork.Instantiate(playerPrefab.name, new Vector3(16, 0, randomPointZ), Quaternion.identity).GetComponent<Shooting>();
            shooting.winEvent += PlayerDeath;
            winnerUi.SetActive(false);
        }
    }

    void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == 0)
        {
            gameOverPanel.SetActive(true);
            winnerUi.SetActive(true);
        }
    }

    public void PlayerDeath(GameObject playerDead)
    {
        playerDead.GetComponent<PlayerSetup>().GetComponent<Camera>().transform.parent = null;
        playerDead.GetComponent<Shooting>().enabled = false;

        string nickName = photonView.Owner.NickName;
        int viewId = photonView.ViewID;

        object[] data = new object[] { nickName, viewId };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOption = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent(0, data, raiseEventOptions, sendOption);
    }

    public void OnQuitButtonClicked()
    {
        PhotonNetwork.LoadLevel("LobbyScene");
    }

}
