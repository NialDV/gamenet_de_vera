﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : StateMachineBehaviour
{
    GameObject NPC;
    GameObject[] wayPoints;

    int currentWaypoint;

    //void Awake()
    //{
    //    wayPoints = GameObject.FindGameObjectsWithTag("waypoint");
    //}

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        NPC = animator.gameObject;
        currentWaypoint = 0;
        //wayPoints = NPC.GetComponent<Enemy>().wayPoints;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (wayPoints.Length == 0) return;



        if (Vector3.Distance(wayPoints[currentWaypoint].transform.position,
            NPC.transform.position) < 25.0f)
        {
            currentWaypoint++;


            if (currentWaypoint >= wayPoints.Length)
            {
                currentWaypoint = 0;
            }
        }

        // rotate
        var direction = wayPoints[currentWaypoint].transform.position - NPC.transform.position;
        NPC.transform.rotation = Quaternion.Slerp(NPC.transform.rotation,
                                                  Quaternion.LookRotation(direction),
                                                  1.0f * Time.deltaTime);

        NPC.transform.position += (Vector3.forward * Time.deltaTime * 15);

        Debug.Log(currentWaypoint);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }


}
