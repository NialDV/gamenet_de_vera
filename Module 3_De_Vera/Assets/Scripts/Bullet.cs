﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Bullet : MonoBehaviourPunCallbacks
{
    public GameObject owner;
    public float movement = 5.0f;
    public float damage = 5f;

    public void OnTriggerEnter(Collider col)
    {
        if (col.gameObject == owner) return;
        //if (col.GetComponent<Shooting>()) return;
        Debug.Log("ball hit");
        if (col.gameObject.CompareTag("Player") && !col.gameObject.GetComponent<PhotonView>().IsMine)
        {
            col.GetComponentInParent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 5);
            Destroy(this.gameObject);
        }
    }
}
