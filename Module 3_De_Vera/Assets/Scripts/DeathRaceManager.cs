﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class DeathRaceManager : MonoBehaviourPunCallbacks
{
    public GameObject[] vehiclePrefab;
    public Transform[] startingPositions;
    public GameObject finisherTextUi;
    public GameObject loserTextUi;
    public int playerCount;
    public static DeathRaceManager instance = null;

    public Text timeText;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            object playerSelectionNumber;
            playerCount = PhotonNetwork.CountOfPlayers;

            finisherTextUi.SetActive(false);
            loserTextUi.SetActive(false);

            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber))
            {
                Debug.Log((int)playerSelectionNumber);

                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Transform position = startingPositions[actorNumber - 1];
                Vector3 instantiatePosition = startingPositions[actorNumber - 1].position;
                PhotonNetwork.Instantiate(vehiclePrefab[(int)playerSelectionNumber].name, instantiatePosition, position.rotation);
            }
        }
    }
}