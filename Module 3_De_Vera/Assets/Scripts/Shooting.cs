﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class Shooting : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject bullet;
    public GameObject turret;

    public int playerCount;

    public enum FireTypes
    {
        raycast,
        projectile
    }

    [Header("HP Related Stuff")]
    public float startHealth = 100;
    private float health = 100;
    public Image healthBar;

    // Start is called before the first frame update
    void Start()
    {
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
        playerCount = PhotonNetwork.CountOfPlayers;
    }

    // Update is called once per frame
    void Update()
    {
        playerCount = PhotonNetwork.CountOfPlayers;

        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit, 200))
            {
                Debug.Log(hit.collider.gameObject.name);
            }

            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 5);
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            // PhotonNetwork.instantiate
            GameObject projectile = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
            projectile.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 1000);
            projectile.GetComponent<Bullet>().owner = this.gameObject;
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.health -= damage;
        this.healthBar.fillAmount = health / startHealth;

        if (health <= 0)
        {
            PlayerDeath();
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
        }
    }

    public enum RaiseEventsCode
    {
        WhoDiedEventCode = 0,
        WhoWonEventCode = 1
    }

    void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)RaiseEventsCode.WhoWonEventCode)
        {
            object[] data = (object[])photonEvent.CustomData;

            string nickNameOfFinishedPlayer = (string)data[0];
            int viewId = (int)data[1];

            DeathRaceManager.instance.finisherTextUi.SetActive(true);
            DeathRaceManager.instance.loserTextUi.SetActive(true);

            Text text = DeathRaceManager.instance.finisherTextUi.GetComponent<Text>();
            text.text = "You have Won " + nickNameOfFinishedPlayer + " !";
        }
        else if (photonEvent.Code == (byte)RaiseEventsCode.WhoDiedEventCode)
        {
            playerCount--;
            object[] data = (object[])photonEvent.CustomData;

            string nickNameOfFinishedPlayer = (string)data[0];
            int viewId = (int)data[1];
            playerCount = (int)data[2];

            DeathRaceManager.instance.loserTextUi.SetActive(true);
            DeathRaceManager.instance.finisherTextUi.SetActive(false);

            if (playerCount <= 1)
            {
                PlayerWon();
            }

            Text text = DeathRaceManager.instance.loserTextUi.GetComponent<Text>();
            text.text = "You have Died " + nickNameOfFinishedPlayer + " !";
        }
    }

    public void PlayerDeath()
    {
        GetComponent<PlayerSetup>().camera.transform.parent = null;
        GetComponent<VehicleScript>().enabled = false;

        string nickName = photonView.Owner.NickName;
        int viewId = photonView.ViewID;

        object[] data = new object[] { nickName, viewId, playerCount };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOption = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.WhoDiedEventCode, data, raiseEventOptions, sendOption);
    }

    public void PlayerWon()
    {
        GetComponent<PlayerSetup>().camera.transform.parent = null;
        GetComponent<VehicleScript>().enabled = false;

        string nickName = photonView.Owner.NickName;
        int viewId = photonView.ViewID;

        object[] data = new object[] { nickName, viewId };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOption = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.WhoWonEventCode, data, raiseEventOptions, sendOption);
    }
}
